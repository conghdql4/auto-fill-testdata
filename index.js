

var elem = document.createElement("div");
var rootId = '_zzz';
var rootIdSelector = '#_zzz';
elem.id = rootId;
document.body.insertBefore(elem,document.body.childNodes[0]);


var mainIframe = "<iframe id='_mainIframePortal' style='display:none'></iframe>";

function numberFormat(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function load_template(){
    var con = document.querySelectorAll(rootIdSelector)[0];
    var xhr = new XMLHttpRequest();
    xhr.open("GET", chrome.extension.getURL("../template/index.html"), true);
    xhr.setRequestHeader('Content-type', 'text/html');
    xhr.send();
    xhr.onreadystatechange = function(e) {
        if(xhr.readyState == 4 && xhr.status == 200) {
            con.innerHTML = xhr.responseText+mainIframe;
            init();
        }
    };
}
load_template();

/**
 * Created by CongHD on 2020/6/30.
 */

function showModal(){
    $('#_mainModal').modal({
        escapeClose: false,
        clickClose: false,
        showClose: false
    });
}
var inputData = [];

function init(){
    
    $(document).on('click', '#download_file', function () {
        downloadDataTestFile();
    });

    $(document).on('click', '#fill-data', function () {
        loadFile();
    });
}
function loadFile() {
    var input, file, fr;
    let caseNumber = $('input[name="case_number"]').val();
    if(caseNumber == "") {
        alert("Input your data test case.");
        return;
    }

    if (typeof window.FileReader !== 'function') {
        alert("The file API isn't supported on this browser yet.");
        return;
    }

    input = document.getElementById('test_data');
    if (!input) {
        alert("Um, couldn't find the fileinput element.");
        return;
    }
    else if (!input.files) {
        alert("This browser doesn't seem to support the `files` property of file inputs.");
        return;
    }
    else if (!input.files[0]) {
        alert("Please select a file before clicking 'Load'");
        return;
    }
    else {
        file = input.files[0];
        fr = new FileReader();
        fr.onload = receivedText;
        let text = fr.readAsText(file);
    }

    function receivedText() {
        result = fr.result;
        
        autoFill(result, caseNumber);
    }

 
}

function downloadDataTestFile() {
    let itemInputs = getItem();
    let csv = "";
    let row1 = [];
    let row2 = [];
    let row3 = [];
    itemInputs.map((item) => {
        row1.push(item.name);
        row2.push(item.type);
        row3.push(item.val);
    })

    //first row
    csv += 'Case No,';
    csv += row1.join(',');
    csv += "\n";
    
    //second row
    csv += "Type,";
    csv += row2.join(',');
    csv += "\n";

    //test data 1
    csv += "Case 1,";
    csv += row3.join(',');
    csv += "\n";

    var hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    hiddenElement.target = '_blank';
    hiddenElement.download = 'TestData.csv';
    hiddenElement.click();
}

function getItem(){
    let itemInputs = [];
    
    $('input,select').map((index,item) => {
        if($(item).is(":visible")) {

            let itemInput = {};
            itemInput.name = $(item).attr('name');
            if($(item).prop("tagName") == "INPUT") {
				if(itemInput.name != "test_data" && itemInput.name != "case_number") {
					itemInput.type = $(item).attr('type') ? $(item).attr('type') : "text" ;
					itemInput.val = $(item).val();
					if(itemInput.type =="radio") {
						itemInput.val = $(`input[name="${itemInput.name}"]:checked`).val();

					} else if(itemInput.type =="checkbox") {
						 if($(item).is(":checked")) {
							 itemInput.val = "1"
						 } else {
							 itemInput.val = "0"
						 }
					}
					let itemFind = itemInputs.find(item => item.name == itemInput.name);
					if(!itemFind) { 
						itemInputs.push(itemInput); 
					}
				}
            } else {
                itemInput.type = 'select' ;
                itemInput.val = $(item).val();
                let itemFind = itemInputs.find(item => item.name == itemInput.name);
                if(!itemFind) { 
                    itemInputs.push(itemInput); 
                }
            }
        }
        
    })
    return itemInputs;
}


function autoFill(data, dataRow){
    dataRow = parseInt(dataRow)  + 1;
    let allTextLines = data.split(/\r\n|\n/);
    console.log();
    if(allTextLines.length  <= dataRow + 1) {
        alert('Data test case ' + (dataRow-1) + ' is not exist');
        return;
    }
    let listItemName = allTextLines[0].split(",");
    let listItemType = allTextLines[1].split(",");
    let listItemVal = allTextLines[dataRow].split(",");
    let numberItem = listItemName.length;
    for(var i = 1;i < numberItem;i++){
        if(listItemType[i]=="text"){
            $(`input[name="${listItemName[i]}"]`).val(listItemVal[i])
        } else if(listItemType[i]=="checkbox"){
            if(listItemVal[i] == "1") {
                $(`input[name="${listItemName[i]}"]`).prop('checked', true);
            } else {
                $(`input[name="${listItemName[i]}"]`).prop('checked', false);
            }
        } else if(listItemType[i]=="radio"){
            if(listItemVal[i] != "") {
                $(`input:radio[name="${listItemName[i]}"][value=${listItemVal[i]}]`).attr('checked', true);    
            }
        } else if(listItemType[i]=="select") {
            $(`select[name="${listItemName[i]}"]`).val(listItemVal[i])
        }
    }
 }
